package main

import (
	"bufio"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"io"
	"log"
	"os"
	"strings"
)

var (
	ipAddress  string /* ip-address of Redis server */
	port       string /* port of Redis server */
	password   string /* password of Redis server */
	typeQuery  string /* type of query to Redis server */
	keyQuery   string /* key of query to Redis server */
	valueQuery string /* value of query to Redis server */
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to initialize variables */
func initVariables() {

	/* initialize using default values */
	ipAddress = "localhost"
	port = "6379"
	password = "Moskow2012"
	typeQuery = "get"
	keyQuery = "password"
	valueQuery = ""

}

/* function to read configuration file */
func readFile(filename string) {

	var (
		reader    *bufio.Reader
		file      *os.File
		err       error
		line      []byte
		row       string
		arrayRows []string
	)

	/* open file */
	file, err = os.Open(filename)
	handleError(err)

	/* create reader and read file */
	reader = bufio.NewReader(file)
	for {

		/* read one line */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		/* convert to string */
		row = string(line)

		/* check whether is comment row */
		if row[0] == '#' {
			continue
		}

		/* parse content of file */
		arrayRows = strings.Split(row, "=")
		if arrayRows[0] == "ipAddress" {
			ipAddress = arrayRows[1]
		} else if arrayRows[0] == "port" {
			port = arrayRows[1]
		} else if arrayRows[0] == "password" {
			password = arrayRows[1]
		} else if arrayRows[0] == "typeQuery" {
			typeQuery = arrayRows[1]
		} else if arrayRows[0] == "keyQuery" {
			keyQuery = arrayRows[1]
		} else if arrayRows[0] == "valueQuery" {
			valueQuery = arrayRows[1]
		}

	}


}

/* function to create connection */
func createConnection() *redis.Conn {

	var (
		err  error
		conn redis.Conn
	)

	/* create connection to server */
	conn, err =	redis.Dial("tcp", ipAddress + ":" + port)
	handleError(err)

	/* authenticate for the server */
	if password != "" {
		_, err = conn.Do("auth", "Moskow2012")
		handleError(err)
	}

	/* return connection */
	return &conn

}

/* function to do action with Redis server */
func doAction(conn *redis.Conn) {

	var (
		err error
	)

	/* check whether is "set" query */
	if typeQuery == "set" {
		/* execute "set" query */
		_, err = (*conn).Do("set", keyQuery, valueQuery)
		fmt.Println("New key with value was added!")
		handleError(err)
	}

	/* check whether is "get" query */
	if typeQuery == "get" {
		/* execute "get" query */
		valueQuery, err = redis.String((*conn).Do("get", keyQuery))
		fmt.Println("Values of key " + keyQuery + " was received: " + valueQuery)
		handleError(err)
	}

}

/* function to close connection */
func closeConnection(conn *redis.Conn) {

	var (
		err error
	)

	/* close connection */
	err = (*conn).Close()
	handleError(err)

}

/* entry point */
func main() {

	var (
		conn      *redis.Conn
		filename  string
	)

	/* initialize variables */
	initVariables()
	/* receive filename from command prompt */
	filename = os.Args[1]

	/* read configuration file */
	readFile(filename)

	/* create connection */
	conn = createConnection()
	/* do action */
	doAction(conn)
	/* close connection */
	closeConnection(conn)

}